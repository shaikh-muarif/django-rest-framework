from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.conf import settings
from .models import TestUser

@receiver(post_save, sender=TestUser)
def send_user_registration_notification(sender, instance, created, **kwargs):
    if created:
        print("a new test user added")
        # try:
        #     subject = 'New User Registration'
        #     message = f'Hi admin,\n\nA new user {instance.username} with email {instance.email} has registered.'
        #     sender_email = settings.EMAIL_HOST_USER
        #     recipient_list = ['']
        #     send_mail(subject, message, sender_email, recipient_list)
        # except Exception as e:
        #     print(e)
