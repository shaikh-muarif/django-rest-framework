from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
# from rest_framework.pagination import PageNumberPagination
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken

from .serializers import BookSerializer, AuthorSerializer, GenreSerializer, EmployeeSerializer, DepartmentSerializer, UserSerializer, TestUserSerializer, ProductSerializer
from .models import Book, Author, Genre, Employee, Department, TestUser, Product
from .pagination import DefaultPagination
from .filters import EmployeeFilter


class ProductViewSet(ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class TestuserListCreateAPIView(generics.ListCreateAPIView):
    queryset = TestUser.objects.all()
    serializer_class = TestUserSerializer


class RegisterUser(APIView):
    def post(self, request):
        serializer = UserSerializer(data = request.data)

        if not serializer.is_valid():
            return Response({'Errors':serializer.errors})
        serializer.save()
        user = User.objects.get(username=serializer.data['username'])
        # token , _ = Token.objects.get_or_create(user=user)
        refresh = RefreshToken.for_user(user)

        return Response({'status': 200, 'payload': serializer.data, 
                         #'token':str(token)
                         'refresh': str(refresh),
                         'access': str(refresh.access_token),
                         })




# viewset
class EmployeeViewSet(ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_class = EmployeeFilter
    # pagination_class = PageNumberPagination
    pagination_class = DefaultPagination
    search_fields = ['name']
    ordering_fields = ['salary'] 

class DepartmentViewSet(ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer

    # authentication_classes = [TokenAuthentication]
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]



# concrete view
class GenreList(generics.ListCreateAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer
 
class GenreDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer



# class based view
class AuthorList(APIView):
    def get(self, request):
        authors = Author.objects.all()
        serializer = AuthorSerializer(authors, many=True)
        return Response(serializer.data)
    def post(self, request):
        serializer = AuthorSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class AuthorDetail(APIView):
    def get(self, request, id):
        author = get_object_or_404(Author, pk=id)
        serializer = AuthorSerializer(author)
        return Response(serializer.data)
    def put(self, request, id):
        author = get_object_or_404(Author, pk=id)
        serializer = AuthorSerializer(author, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
    def delete(self, request, id):
        author = get_object_or_404(Author, pk=id)
        author.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



# function based view
@api_view(['GET', 'POST'])
def books(request):
    if request.method == 'GET':
        books = Book.objects.select_related('author').all()
        serializer = BookSerializer(books, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = BookSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data)       

@api_view(['GET', 'PUT', 'PATCH', 'DELETE'])
def book_detail(request, id):
    book = get_object_or_404(Book, pk=id)
    if request.method == 'GET':
        serializer = BookSerializer(book)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = BookSerializer(book, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
    elif request.method == 'PATCH':
        serializer = BookSerializer(book, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
    elif request.method == 'DELETE':
        book.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    

