from django.urls import path, include
# from rest_framework.routers import SimpleRouter, DefaultRouter
from rest_framework_nested import routers
from . import views

router = routers.DefaultRouter()

router.register(r'department', views.DepartmentViewSet, basename="department")
router.register(r'employee', views.EmployeeViewSet, basename="employee")
router.register(r'product', views.ProductViewSet, basename="product")

# urlpatterns = router.urls

# Nested routers for employee under department
department_router = routers.NestedSimpleRouter(router, 'department', lookup='department')
department_router.register(r'employee', views.EmployeeViewSet, basename='department-employee')

urlpatterns = [
    path('book/', views.books, name='books-list'),
    path('book/<int:id>/', views.book_detail, name='book-detail'),
    path('author/', views.AuthorList.as_view(), name="authors-list"),
    path('author/<int:id>/', views.AuthorDetail.as_view(), name="author-detail"),
    path('genre/', views.GenreList.as_view(), name="genre-list"),
    path('genre/<int:pk>/', views.GenreDetail.as_view(), name="genre-detail"),
    path('', include(router.urls)),
    path('', include(department_router.urls)),
    path('register/', views.RegisterUser.as_view()),
    path('test-users/', views.TestuserListCreateAPIView.as_view(), name='testuser-list-create'),
]

