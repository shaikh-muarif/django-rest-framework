from django.contrib import admin
from .models import Book, Author, Genre, Employee, Department, TestUser

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Employee)
admin.site.register(Department)
admin.site.register(TestUser)